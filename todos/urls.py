from django.urls import path

from todos.views import (
    TodoItemCreateView,
    TodoListCreateView,
    TodoListDetailView,
    TodoListListView,
    TodoListDeleteView,
    TodoListUpdateView,
)


urlpatterns = [
    path("", TodoListListView.as_view(), name="todo_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todo_detail"),
    path("items/create/", TodoItemCreateView.as_view(), name="todoitem_new"),
    path("create/", TodoListCreateView.as_view(), name="todolist_new"),
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="todo_delete"),
    path("<int:pk>/edit/", TodoListUpdateView.as_view(), name = "todo_edit"),
]