# Generated by Django 4.0.3 on 2022-03-24 22:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('todos', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='todolist',
            old_name='name',
            new_name='list_names',
        ),
    ]
