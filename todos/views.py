from pyexpat import model
from re import template
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from todos.models import TodoList, TodoItem


# Create your views here.

class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]

    def form_valid(self, form):
        owner = form.save(commit=False)
        owner.user = self.request.user
        owner.save()
        return redirect("todo_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/create.html"
    fields = ["task", "due_date", "list"]
    
    def form_valid(self, form):
        owner = form.save(commit=False)
        owner.user = self.request.user
        owner.save()
        return redirect("todo_list")
 

class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]
    
    def form_valid(self, form):
        owner = form.save(commit=False)
        owner.user = self.request.user
        owner.save()
        return redirect("todo_list")